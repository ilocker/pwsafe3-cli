
TARGET=pwsafe3-cli
CPPFLAGS := -std=c++11 -pedantic -Wall -Wextra -I
LIBS := pwsafe3-git/lib/unicodedebug/libcore.a

.PHONY = all clean

all: pwsafe3-cli

$(TARGET).o: $(TARGET).cpp
	$(CXX) $(CPPFLAGS) -c -o $@ $^

$(TARGET): pwsafe3-cli.o
	$(CXX) $(CPPFLAGS) -o $@ $^ $(LIBS)

clean:
	rm -f *.o $(TARGET)
